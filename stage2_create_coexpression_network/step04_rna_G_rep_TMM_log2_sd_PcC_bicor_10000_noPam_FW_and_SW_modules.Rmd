---
title: "Make modules Host genes all data TMM"
author: "Marius Strand"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Load in libraries and set settings.

```{r message=FALSE, warning=FALSE}
options(stringsAsFactors = FALSE)
library(magrittr)                  # For pipes %>% 
library(cowplot)                   # For combining plots
library(dendextend)                # Modify dendrograms
library(WGCNA)                     # Weighted gene co-expression network analysis
library(dplyr)                     # Manipulation of data frames
library(tidyverse)
library(ggplot2)
theme_set(theme_classic())
theme_update(plot.title = element_text(hjust = 0.5))
theme_update(plot.title = element_text(face="bold"))
library(Ssa.RefSeq.db)             # Provides gene information for the A.salmon genome
#allowWGCNAThreads(nThreads = 8)    # Does not seem to work, only uses one core.
```


## Settings

Document what settings was used or is going to be used:

```{r}
# Set seed as a precaution for reproducibility as some methods are non-deterministic.
set.seed(13118)
Run_analysis <- TRUE    # if FALSE it tries to load data instead of running the module creation step.

save_plot <- TRUE
plot_labeling_size = 20
prefix <- "h"

save_TOM <- FALSE       # Can get rather big
pam_stage <- FALSE      # Partitioning around medoids, tries to put more genes into modules where it is not directly clear from the dendrogram cutting.
set_verbose = 1         # How much detail from the WGCNA functions? A higher number means more detail.

omics_type = "rna"      # Just a name in the form of a string

tissue = "G"            # G for gut, or L for liver

what_samples = "FW_and_SW"     # FW, SW or FW_and_SW

take_average = F        # Take the average of each sample group

####

assoc_measure = "bicor"

max_block_size = 10000      #  13000 ~ 2-2.5 h |

####

applied_norm = "TMM"        #  TPM or (TMM, choose raw counts below)


applied_transf = "log2"     #  log2 or CLR


applied_filter = "sd"       #  sd or mad

pcCorrection <- T
if(pcCorrection){
  estimate_n.pc = T
  if(!estimate_n.pc){
    number_of_pcs_to_remove = 1 # Does not matter when pcCorrection is FALSE
  }
}
```

To keep an overview the result of the analysis is named by the parameters used.
```{r}

save_name <- paste(omics_type,
                   tissue,
                   ifelse(take_average, "avg", "rep"),
                   applied_norm,
                   applied_transf,
                   applied_filter,
                   ifelse(pcCorrection, paste0("PcC", ifelse(estimate_n.pc, "", number_of_pcs_to_remove)), "noPcC"),
                   assoc_measure,
                   max_block_size,
                   ifelse(pam_stage, "pam", "noPam"),
                   what_samples,
                   sep = "_")

paste0("../data/module_data/", save_name, "_modules.RData")

```


Load in data.
```{r}
input_file <- "../data/RData/rna.all.RData"

(input_var <- load(file = input_file))
```

If TMM is chosen as the normalization then we need raw counts.
```{r}
if(applied_norm %in% c("TPM", "TMM")){
  if(applied_norm == "TPM"){
    omics_data <- rna.TPM
  }
  if(applied_norm == "TMM"){
    omics_data <- rna.raw_counts
  }
} else{
  stop("Unknown normalization")
}
```


Input data `omics_data`:
Transcripts of gene isoforms should be summarized to gene counts.
Genes are rows, and samples are columns.


# Subset samples

Load in the subset of samples were outliers have been removed, and samples match between omics data.
```{r}
(load(file = "../data/RData/samples_to_keep.RData"))
```

```{r}
samples_to_keep <- gut_samples_to_keep

if(what_samples == "FW"){
  samples_to_keep <- samples_to_keep[which(grepl(pattern = "FW", samples_to_keep))]
}
if(what_samples == "SW"){
  samples_to_keep <- samples_to_keep[which(grepl(pattern = "SW", samples_to_keep))]
}
# Or no selection

omics_data <- omics_data[, which(colnames(omics_data) %in% samples_to_keep)]
sample_info <- sample_info.rna[which(sample_info.rna$sampleName %in% samples_to_keep),]
all(colnames(omics_data) == sample_info$sampleName)
```

These are all the samples that are used in this analysis:
```{r}
samples_to_keep 
```


# Normalization

TPM is already normalized
TMM calculates normalization factors which are then used to find effective library sizes.
Normalized library sizes are used to calculate Counts Per Million (CPM) values.

```{r}
if(applied_norm != "TPM"){
  report <- "Finding TMM normalization factors and normalizing to CPM values"
  omics_data <- edgeR::DGEList(omics_data)
  omics_data <- edgeR::calcNormFactors(omics_data, method = "TMM")
  omics_data <- edgeR::cpm(omics_data) # Gives the same output as:
  # t(t(dge$counts)/(dge$samples$lib.size * dge$samples$norm.factors) *1e6)
} else{
  report <- "Data already normalized to TPM"
}
report
```


# Averaging

Find the average values for each sample group. It could be useful if there is much noise in the data.
```{r}
if(take_average){
  omics_data %>% 
    t() %>% 
    as.data.frame() %>% 
    tibble::rownames_to_column(var = "sampleName") %>% 
    #dplyr::filter(grepl("FW",sampleName)) %>%
    dplyr::mutate(sampleName = sub("_[0-9]{1,2}", "", sampleName)) %>% 
    dplyr::group_by(sampleName) %>%
    dplyr::summarise_all(.funs = mean) %>%
    dplyr::ungroup() %>%
    tibble::column_to_rownames(var = "sampleName") %>% t() -> omics_data
  
  omics_data <- omics_data[, order(colnames(omics_data))]
  
  sample_info %>% 
    dplyr::select(-c(Tank, Replicate)) %>% 
    dplyr::mutate(sampleName = sub("_[0-9]{1,2}", "", sampleName)) %>% 
    dplyr::distinct() -> sample_info
  
  sample_info <- sample_info[order(sample_info$sampleName), ]
  
  if(!all(colnames(omics_data) == sample_info$sampleName)){
    stop("Something went wrong")
  }
}

```



# Filtering
## Temporary transformation for filtering

Temporary transformation is done to make different transformations more comparable, i.e. filtering can be done the same way even if the transformation is different in the end.
```{r}
omics_data_temp <- log2(omics_data + 1)
```

Histogram plotting function
```{r}
plot_hist <- 
  function(m.data, wfun = "sd", plot_title = "log2 genes"){ 
    if(wfun == "no"){
      m.data %>% 
      c() %>% 
      as.data.frame() %>% 
      ggplot(aes(x = .)) + 
      geom_histogram(binwidth = 1/100, color = "#254466", fill = "#5EACFF") + 
      xlab(paste(wfun, "value")) +
      ggtitle(paste("Expression", "of", plot_title))
    } else{
      m.data %>% 
      apply(1,get(wfun)) %>% 
      data.frame() %>% 
      ggplot(aes(x = .)) + 
      geom_histogram(binwidth = 1/100, color = "#254466", fill = "#5EACFF") + 
      xlab(paste(wfun, "value")) +
      ggtitle(paste(wfun, "of", plot_title))
    }
}
```

```{r fig.height=5, fig.width=10}
cowplot::plot_grid(plot_hist(omics_data_temp, "sd"),
                   plot_hist(omics_data_temp, "mad"),
                   plot_hist(omics_data_temp, "no"),
                   align = "h",
                   ncol = 3)
```

## Filter low counts

From `r dim(omics_data)[1]` genes.

```{r}
max_value <- function(gene_vector, x= 1){max(gene_vector) > x}
idx_keep <- omics_data_temp %>% apply(1, FUN = max_value) %>% which()
omics_data_temp <- omics_data_temp[idx_keep, ]
```

To `r dim(omics_data)[1]` genes.

## Filter low variance

```{r}
if(applied_filter == "sd"){
  var_value <- function(gene_vector){sd(gene_vector) >= 0.15}
}

if(applied_filter == "mad"){
  var_value <- function(gene_vector){mad(gene_vector) > 0}
}

idx_keep <- omics_data_temp %>% apply(MARGIN = 1, FUN = var_value) %>% which()
omics_data_temp <- omics_data_temp[idx_keep, ]
```

```{r fig.height=5, fig.width=10}
cowplot::plot_grid(plot_hist(omics_data_temp, "sd"),
                   plot_hist(omics_data_temp, "mad"),
                   plot_hist(omics_data_temp, "no"),
                   align = "h",
                   ncol = 3)
```


## Remove low expression and low variance genes from untransformed data

```{r}
genes_left <- rownames(omics_data_temp)
idx <- which(rownames(omics_data) %in% genes_left)

omics_data <- omics_data[idx,]
```


# Transformation

```{r}
dim(omics_data) %>% paste(c("Genes", "Samples"))
```

```{r}
if(applied_transf %in% c("log2", "CLR")){
  
  if(applied_transf == "log2"){
    omics_data <- log2(omics_data + 1)
    report <- "Applied log2 transformation"
  } 
  
  if(applied_transf == "CLR"){
    omics_data <-  propr:::proprCLR(t(omics_data) + 1)
    omics_data <- t(omics_data)
    report <- "Applied CLR transformation"
  } 
  } else{
  stop("What transformation?")
}
report
```


```{r fig.height=5, fig.width=10}
cowplot::plot_grid(plot_hist(omics_data, "sd", plot_title = paste(applied_transf, "genes")),
                   plot_hist(omics_data, "mad", plot_title = paste(applied_transf, "genes")),
                   plot_hist(omics_data, "no", plot_title = paste(applied_transf, "genes")),
                   align = "h",
                   ncol = 3)
```


```{r}
dim(omics_data) %>% paste(c("Genes", "Samples"))
```


# PC-correction or not?

```{r}
if(pcCorrection){
  library(sva)
  if(estimate_n.pc){
    
    print(t(omics_data) %>% dim() %>% paste(c("Samples", "genes"))) # samples in rows, variables in columns
    mod=matrix(1,nrow=nrow(t(omics_data)),ncol=1)
    colnames(mod)="Intercept"
    
    print(omics_data %>% dim() %>% paste(c("genes", "Samples"))) # Variables in rows, sample columns
    n.pc = num.sv(dat = omics_data, mod = mod,  method="be")
  } else{
    n.pc = number_of_pcs_to_remove
  }
  
  print(omics_data %>% dim() %>% paste(c("genes", "Samples"))) # Variables in rows, sample columns
  omics_data <- sva_network(omics_data, n.pc = n.pc)
}
```

```{r fig.height=5, fig.width=10}
if(pcCorrection){
  report <- paste0("PC-correction with ", n.pc, " PC(s) removed.")
  
  cowplot::plot_grid(plot_hist(omics_data, "sd"),
                   plot_hist(omics_data, "mad"),
                   plot_hist(omics_data, "no"),
                   align = "h",
                   ncol = 3)
} else{
  report <- "There was no PC-correction performed."
}
report
```


## Save processed data

```{r}
save_data_path <- paste0("../data/transformed_data/", save_name, ".RData")
save(omics_data, identity = save_name, file = save_data_path)
```




# Make modules

## Soft Threshold

Find the soft thresholding power beta to which co-expression similarity is raised to calculate adjacency,
based on the criterion of approximate scale-free topology. 

WGCNA expects samples as rows and genes as columns.

```{r}
omics_data <- omics_data %>% t()
```

```{r}
dim(omics_data) %>% paste(c("Samples", "Genes"))
```


```{r}
powers <- c(c(1:10), seq(from = 12, to=20, by=2))
```

```{r message=FALSE, warning=FALSE}
# Here you have the choice of either a signed network, a signed hybrid or unsigned (default).

if(Run_analysis){
  suppressWarnings(sft <- pickSoftThreshold(omics_data, 
                         powerVector = powers, 
                         verbose = set_verbose, 
                         networkType = "signed",
                         corFn= assoc_measure))
  save(sft, file = paste0("../data/module_data/st_dump/", save_name, "_softThreshold.RData"))
} else{
  load(file = paste0("../data/module_data/st_dump/", save_name, "_softThreshold.RData"))
}

# Find the soft thresholding power beta to which co-expression similarity is raised to calculate adjacency.
# based on the criterion of approximate scale-free topology.

idx <- min(which((-sign(sft$fitIndices[,3])*sft$fitIndices[,2]) > 0.90))
if(is.infinite(idx)){
  idx <- min(which((-sign(sft$fitIndices[,3])*sft$fitIndices[,2]) > 0.80))
  if(!is.infinite(idx)){
    st <- sft$fitIndices[idx,1]
  } else{
    idx <- which.max(-sign(sft$fitIndices[,3])*sft$fitIndices[,2])
    st <- sft$fitIndices[idx,1]
  }
} else{
  st <- sft$fitIndices[idx,1]
}


# Plot Scale independence measure and Mean connectivity measure

# Scale-free topology fit index as a function of the soft-thresholding power
data.frame(Indices = sft$fitIndices[,1],
           sfApprox = -sign(sft$fitIndices[,3])*sft$fitIndices[,2]) %>% 
  ggplot() + 
  geom_hline(yintercept = 0.9, color = "red", alpha = 0.6) + # corresponds to R^2 cut-off of 0.9
  geom_hline(yintercept = 0.8, color = "red", alpha = 0.2) + # corresponds to R^2 cut-off of 0.8
  geom_line(aes(x = Indices, y = sfApprox), color = "red", alpha = 0.1, size = 2.5) +
  geom_text(mapping = aes(x = Indices, y = sfApprox, label = Indices), color = "red", size = 4) +
  ggtitle("Scale independence") +
  xlab("Soft Threshold (power)") +
  ylab("SF Model Fit,signed R^2") +
  xlim(1,20) +
  ylim(-1,1) +
  geom_segment(aes(x = st, y = 0.25, xend = st, yend = sfApprox[idx]-0.05), 
               arrow = arrow(length = unit(0.2,"cm")), 
               size = 0.5)-> scale_independence_plot 
  
 


# Mean connectivity as a function of the soft-thresholding power

data.frame(Indices = sft$fitIndices[,1],
           meanApprox = sft$fitIndices[,5]) %>% 
  ggplot() + 
  geom_line(aes(x = Indices, y = meanApprox), color = "red", alpha = 0.1, size = 2.5) +
  geom_text(mapping = aes(x = Indices, y = meanApprox, label = Indices), color = "red", size = 4) +
  xlab("Soft Threshold (power)") +
  ylab("Mean Connectivity") +
  geom_segment(aes(x = st-0.4, 
                   y = sft$fitIndices$mean.k.[idx], 
                   xend = 0, 
                   yend = sft$fitIndices$mean.k.[idx]),
               arrow = arrow(length = unit(0.2,"cm")), 
               size = 0.4) +
  ggtitle(paste0("Mean connectivity: ", 
                 round(sft$fitIndices$mean.k.[idx],2))) -> mean_connectivity_plot


cowplot::plot_grid(scale_independence_plot, mean_connectivity_plot, ncol = 2, align = "h", labels = c("A", "B"), label_size = plot_labeling_size) -> si_mc_plot

si_mc_plot
```

```{r}
st
```


## Block-wise network construction and module detection

The function `blockwiseModules` will first pre cluster with fast crude clustering method to cluster genes into blocks not exceeding the maximum, blocks may therefore not be fully optimal in the end.

Change the parameters here to better reflect your own data.
```{r, warning=FALSE}
if(Run_analysis){
  modules.omics <- blockwiseModules(omics_data, 
                          maxBlockSize = max_block_size,
                          power = st, 
                          networkType = "signed", 
                          TOMType = "signed",
                          corType = assoc_measure,
                          maxPOutliers = 0.05,
                          deepSplit = 4, # Default 2
                          minModuleSize = 3,           # 30
                          minCoreKME = 0.5,            # Default 0.5
                          minCoreKMESize = 2,          # Default minModuleSize/3,
                          minKMEtoStay = 0.5,          # Default 0.3
                          reassignThreshold = 0,       # Default 1e-6
                          mergeCutHeight = 0.5,        # Default 0.15
                          pamStage = pam_stage, 
                          pamRespectsDendro = TRUE,
                          replaceMissingAdjacencies = TRUE,
                          #nThreads = 8,
                          numericLabels = TRUE,
                          saveTOMs = save_TOM,
                          saveTOMFileBase = paste0("../data/transformed_data/TOMs/", save_name),
                          verbose = set_verbose)
  
  rownames(modules.omics$MEs) <- rownames(omics_data)
  names(modules.omics$colors) <- colnames(omics_data)
  names(modules.omics$unmergedColors) <- colnames(omics_data)
  
  hubs <- chooseTopHubInEachModule(omics_data, modules.omics$colors, power = st, omitColors = "0")
  
  #####
  
  stage2results <- list(modules = modules.omics, 
                        hubs = hubs, 
                        input_file = input_file, 
                        output_data = save_data_path,
                        identity = save_name)
  
  save(stage2results, 
       file = paste0("../data/module_data/", save_name, "_modules.RData"))
  
} else {
  load(file = paste0("../data/module_data/", save_name, "_modules.RData"))
}

```


# Visualizing module characteristics

## Description

```{r}
# Convert labels to colors for plotting
merged_colors <- labels2colors(stage2results$modules$colors)
```

```{r include=FALSE}
n_modules <- unique(merged_colors) %>% length()

samples_good <- sum(stage2results$modules$goodSamples) == length(stage2results$modules$goodSamples)
genes_good <- sum(stage2results$modules$goodGenes) == length(stage2results$modules$goodGenes)

ME_good <- sum(stage2results$modules$MEsOK) == length(stage2results$modules$MEsOK)
```
`r ifelse(samples_good, "All samples are OK.","Not all samples are OK.")`  
`r ifelse(genes_good, "All genes are OK.","Not all genes are OK.")`  

There where `r n_modules` modules found.  
`r ifelse(ME_good, "All module eigengenes are OK.","Not all module eigengenes are OK.")`  

How many genes are there in each module?
```{r fig.height=5, fig.width=5}
table(stage2results$modules$colors) %>% 
  as.data.frame() %>% 
  dplyr::rename(Module = Var1, Size = Freq) %>%
  dplyr::mutate(Module_color = labels2colors(as.numeric(as.character(Module)))) -> module_size

if (FALSE) { # Remove modules without significant associations.
  load(file = "../sig_modules.Rdata")
  sig.modules <- as.numeric(gsub("ME","", sig.modules))
  module_size <- module_size[module_size$Module %in% c(sig.modules,0),]
}
if (TRUE) { # Select only largest modules
  module_size <- module_size[1:49,]
}

module_size %>% 
  ggplot(aes(x = Module, y = Size, fill = Module)) +
  geom_col(color =  "#000000") +
  ggtitle("Number of genes in each module") +
  theme(legend.position = "none") + 
  scale_fill_manual(values = setNames(module_size$Module_color,module_size$Module)) +
  geom_text(aes(label = Size),vjust = 0.5, hjust = -0.18, size = 3.5) +
  ylim(0, max(module_size$Size)*1.1) +
  theme(plot.margin = margin(2, 2, 2, 2, "pt")) +
  coord_flip()-> module_size_barplot

module_size_barplot

```


## Dendrogram and module colors

```{r fig.height=5, fig.width=10}
# Plot the dendrogram and the module colors underneath for each block
for(i in seq_along(stage2results$modules$dendrograms)){
  plotDendroAndColors(stage2results$modules$dendrograms[[i]], merged_colors[stage2results$modules$blockGenes[[i]]],
                      "Module colors",
                      dendroLabels = FALSE, hang = 0.03,
                      addGuide = TRUE, guideHang = 0.05,
                      main = paste0("Cluster Dendrogram\n", 
                                    "for block ", 
                                    i,": ",
                                    length(stage2results$modules$blockGenes[[i]]),
                                    " genes"))
}
```


## Module (Eigengene) correlation

```{r}
MEs <- stage2results$modules$MEs


# Module correlation to other modules
MEs_R <- bicor(MEs, MEs, maxPOutliers = 0.05)

idx.r <- which(rownames(MEs_R) == "ME0")
idx.c <- which(colnames(MEs_R) == "ME0")

MEs_R_noME0 <- MEs_R[-idx.r, -idx.c]
```

```{r fig.height=5, fig.width=10}
MEs_R_noME0[upper.tri(MEs_R_noME0)] %>% 
  as.data.frame() %>% 
  dplyr::rename("correlation" = ".") %>% 
  ggplot(aes(correlation)) + 
  geom_histogram(bins = 20) +
  #geom_density() + 
  xlim(-1, 1) +
  ggtitle(paste0(prefix,"ME correlation\n w/o ",prefix ,"ME0")) -> MEs_R_density

pheatmap::pheatmap(MEs_R_noME0, color = colorRampPalette(c("Blue", "White", "Red"))(100),
                   silent = T, 
                   breaks = seq(-1,1,length.out = 101),
                   treeheight_row = 10, 
                   treeheight_col = 10,
                   main = paste0(prefix,"ME correlation heatmap w/o ", prefix ,"ME0"),
                   labels_row = rep("", length(rownames(MEs_R))), #paste0(prefix, rownames(MEs_R)),
                   labels_col = rep("", length(colnames(MEs_R))) #paste0(prefix, colnames(MEs_R))
                   ) -> MEs_R_Corr

cowplot::plot_grid(MEs_R_density, MEs_R_Corr$gtable, labels = c("D", "E"), label_size = plot_labeling_size, rel_widths = c(0.6, 1)) -> density_eigen

density_eigen
```

```{r message=FALSE, warning=FALSE}
all(rownames(omics_data) == rownames(MEs))
dim(omics_data) %>% paste0(c(" samples", " genes"))
kME <- bicor(omics_data, MEs, maxPOutliers = 0.05)
dim(kME) %>% paste0(c(" genes", " modules"))
```


<!-- Show a plot of the intra modular correlation; How the genes within a module correlates to the module eigengene. -->

<!-- ```{r} -->
<!-- intra_cor <- c() -->
<!-- for (i in 1:ncol(omics_data)) { -->
<!--   m <- stage2results$modules$colors[i] -->
<!--   if(m != 0){ -->
<!--     intra_cor[i] <- kME[i, paste0("ME", m)] -->
<!--   } else{ -->
<!--     intra_cor[i] <- NA -->
<!--   } -->

<!-- } -->

<!-- idx <- which(is.na(intra_cor)) -->
<!-- intra_cor <- intra_cor[-idx] -->

<!-- plot(density(intra_cor), main = "Correlations with module eigengene (within module correlation)\n without ME0", xlim = c(-1,1)) -->
<!-- ``` -->


Show the correlation to the eigengene; How the genes within a module correlate to the module eigengene.
Show for each module individually colored by module color.

```{r}
# Corr within modules
corr_within_module <- function(omics_data, modules, module_x = 1){
  idx.omics_data <- which(modules$colors == module_x)
  idx.me <- which(colnames(modules$MEs) == paste0("ME",module_x))
  kME_x <- bicor(omics_data[,idx.omics_data], modules$MEs[,idx.me], maxPOutliers = 0.05)
  kME_x
}

ggplot.list <- list()

modules <- 
colnames(stage2results$modules$MEs)[colnames(stage2results$modules$MEs) %>% sub("ME", "", .) %>% as.numeric() %>% order()]

for(m in modules[1:49]){
  h <- as.numeric(sub("ME","", m))
  data.frame(x = suppressWarnings(corr_within_module(omics_data = omics_data, modules = stage2results$modules, module_x = h))) %>% 
    ggplot() + 
    #geom_density(aes(x = x), fill = labels2colors(h), color = "black", alpha = 0.5) +
    geom_histogram(aes(x), fill = labels2colors(h), color = "black", alpha = 0.5, bins = 20) + 
    xlim(-1, 1) +
    xlab("Gene correlation") +
    ggtitle(paste0(prefix,m)) -> da_plot
  
  ggplot.list[[m]] <- da_plot
}

ggplot.list <- ggplot.list[ggplot.list %>% names() %>% sub("ME", "", .) %>% as.numeric() %>% order()]
```

```{r fig.height=20, fig.width=20}
cowplot::plot_grid(plotlist = ggplot.list) -> density_all_plot

density_all_plot
```

# Combine to one plot

```{r fig.height=20, fig.width=15}
cowplot::plot_grid(si_mc_plot , density_eigen, ncol = 1, rel_heights = c(0.8,1)) -> part_1


cowplot::plot_grid(part_1, module_size_barplot, labels = c("", "C"), label_size = plot_labeling_size, rel_widths = c(1,0.5)) -> part_2


cowplot::plot_grid(part_2, density_all_plot, ncol = 1, rel_heights = c(0.8,1), labels = c("", "F"), label_size = plot_labeling_size)
```

```{r}
if(save_plot){
  ggsave(filename = paste0("../images/Module_diagnostics/",
                           "Module_diagnostics_",save_name, ".pdf"), device =
           "pdf", height = 20, width = 15)
}
```



# Hub genes

For each module it is possible to pick a hub gene with the function `chooseTopHubInEachModule`.


```{r}
stage2results$hubs %>% 
  as.data.frame() %>% 
  dplyr::rename("gene_id" = ".") %>% 
  tibble::rownames_to_column(var = "Module") -> top_genes
dplyr::left_join(top_genes, Ssa.RefSeq.db::get.genes(top_genes$gene_id, match = T), by = "gene_id") -> top_genes # NB! Only the longest transcript has been picked by the get.genes function.

top_genes
```


# Hub centrality

```{r}

degrees <- intramodularConnectivity.fromExpr(omics_data, colors = stage2results$modules$colors, 
                                             power = st,
                                             networkType = "signed", 
                                             distFnc = chosen_parameter_set$assoc_measure)

degrees$gene_id <- colnames(omics_data)
degrees$Module <- stage2results$modules$colors
degrees <- degrees[,c(6,5,1:4)]
info <- get.genes(colnames(omics_data), match = TRUE)
degrees$symbol <- info$gene
degrees$gene_description <- info$product

write.table(degrees, file = paste0("../export/", save_name, "_degrees.txt"), 
            quote = FALSE, sep = "\t", row.names = FALSE)
```
