---
title: "Correlate gene modules with OTU modules and traits/external variables"
author: "Marius Strand"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

load in libraries
```{r}
library(magrittr) # For pipes %>% 
library(dplyr)    # For data frame manipulations
library(tibble)   # For rownames_to_columns etc
library(tidyr)    # For functions: Separate, ...
```

File paths RData:
```{r}
metagenomics_path <- "../data/module_data/otu_G_rep_0.005_CSS_log2_bicor_PcC_10000_noPam_FW_and_SW_modules.RData"

transcriptomics_path <- "../data/module_data/rna_G_rep_TMM_log2_sd_PcC_bicor_10000_noPam_FW_and_SW_modules.RData"
```


Correlate every sample, or take the average of replicates within each condition/sample group?
```{r}
take_average = T
cluster_the_columns = F
col_cut <- 1
row_cut <- 1
prefix_genes <- "h"
prefix_OTUs <- "m"
```

# Transcriptomics

load in transcriptomics
```{r}
(load(file = transcriptomics_path)) # The gene network modules
```
```{r}
X <- stage2results

# Remove ME0 from analysis
idx <- which(colnames(X$modules$MEs) == "ME0")
X$modules$MEs <- X$modules$MEs[,-idx]
filter.sig.mod <- 2
if (filter.sig.mod == 2) { # Remove modules without significant associations. Hack!
  load(file = "../data/module_data/sig_modules.Rdata")
  
  idx <- c()
  for (m in sig.modules.h) {
    idx <- c(idx, which(colnames(X$modules$MEs) == m))
  }
  X$modules$MEs <- X$modules$MEs[,idx]
}

if(take_average){
  X$modules$MEs %>% 
  rownames_to_column(var = "sampleName") %>%
  mutate(sampleName = sub("_[0-9]{1,2}$", "", sampleName)) %>% 
  group_by(sampleName) %>% 
  summarise_all(list(mean)) %>% 
  ungroup() %>% 
  column_to_rownames(var = "sampleName") -> X_eigengenes
  
  X_eigengenes <- as.data.frame(X_eigengenes)
  
} else{
  X_eigengenes <- X$modules$MEs
}
```

```{r}
# Create a dendrogram of the transcriptomics eigengenes to organise the final plots.
X_ME_dendro <- hclust(as.dist(1 - WGCNA::bicor(X_eigengenes, maxPOutliers = 0.05)), method = "ward.D2")

# "..palettes that vary from one hue to another via white may have a more symmetrical appearance in RGB space"
if(take_average){
  heatmap_colors <- colorRampPalette(c("#18b29f","#FFFFFF","#ac6721"), interpolate = "spline", space = "rgb")(51)
} else{
  heatmap_colors <- colorRampPalette(c("#18b29f","#FFFFFF","#ac6721"), interpolate = "spline", space = "rgb")(51)
}


annotation_col <- 
  rownames(X_eigengenes) %>% 
  as.data.frame() %>% 
  dplyr::rename(sampleName = ".") %>% 
  tidyr::separate(col = sampleName, into = c("Tissue", "Water", "Day", "Feed"), remove = F, extra = "drop") %>% # If replicates are present, we drop them here
  dplyr::mutate(Feed = factor(Feed, levels = c("FO", "VOFO", "FOVO", "VO"))) %>% 
  dplyr::mutate(Day = sub("D","", Day)) %>% 
  dplyr::mutate(Days_total = ifelse(Water == "FW", paste0("FW", Day), paste0("SW", Day))) %>%
  dplyr::mutate(Days_total = factor(Days_total, levels= c(paste0("FW", 0:20), paste0("SW", 0:20)))) %>%
  dplyr::mutate(Day = as.numeric(Day), Days_total = as.numeric(Days_total)) %>% 
  dplyr::select(sampleName,Feed, Water, Day, -Days_total) %>%  
  # Order here is reflected in the order of annotations, Days_total = new number for each unique day
  tibble::column_to_rownames(var = "sampleName")

annotation_colors <- list(
  Feed = c(`FO` = "#F08A46", `VO` = "#8EB470", `FOVO` = "#B7CFA4", `VOFO` = "#F6B890"),
  Water = c(`FW` = "#1D88AF", `SW` = "#12556D"),
  Day = gray.colors(length(unique(annotation_col$Day)), start = 0.9, end = 0.6),
  Days_total = gray.colors(length(unique(annotation_col$Days_total)), start = 0.9, end = 0.3)
)
```



```{r fig.height=4, fig.width=12}
X_eigengenes_to_plot <- 
  dplyr::inner_join(annotation_col %>% 
               rownames_to_column(var = "sampleName"), 
             X_eigengenes %>% 
               rownames_to_column(var = "sampleName"), 
             by = "sampleName") %>%
  dplyr::arrange(Feed, Water, Day) %>%              # The order at which the columns should appear, given that there is no clustering.
  dplyr::select(sampleName, starts_with("ME")) %>% 
  tibble::column_to_rownames(var = "sampleName") %>% 
  t()

pheatmap::pheatmap(X_eigengenes_to_plot, 
                   cluster_cols = cluster_the_columns,
                   cluster_rows = X_ME_dendro,
                   treeheight_row = 20,
                   cutree_rows = row_cut,
                   cutree_cols = col_cut,
                   color = heatmap_colors,
                   fontsize = 10,
                   fontsize_col = ifelse(take_average, 10, 6),
                   fontsize_row = 8,
                   annotation_colors = annotation_colors,
                   annotation_col = annotation_col, 
                   silent = F,
                   labels_row = paste0(prefix_genes, rownames(X_eigengenes_to_plot)),
                   main = paste("Gene Module 'expression'\n", ifelse(take_average, "mean values", ""))) -> X_plot
```

# Correlate modules from transcriptomics and metagenomics.

Load in metagenomics.

```{r}
(load(file = metagenomics_path))       # The OTU network modules
```

```{r}
Y <- stage2results

# Remove ME0 from analysis
idx <- which(colnames(Y$modules$MEs) == "ME0")
Y$modules$MEs <- Y$modules$MEs[,-idx]
if (filter.sig.mod == 2) { # Remove modules without significant associations. Hack!
  load(file = "../data/module_data/sig_modules.Rdata")
  
  idx <- c()
  for (m in sig.modules.m) {
    idx <- c(idx, which(colnames(Y$modules$MEs) == m))
  }
  Y$modules$MEs <- Y$modules$MEs[,idx]
}

if(take_average){
  Y$modules$MEs %>% 
  rownames_to_column(var = "sampleName") %>% 
  mutate(sampleName = sub("_[0-9]{1,2}$", "", sampleName)) %>% 
  group_by(sampleName) %>% 
  summarise_all(list(mean)) %>% 
  ungroup() %>% 
  column_to_rownames(var = "sampleName") -> Y_eigengenes
  
  Y_eigengenes <- as.data.frame(Y_eigengenes)
} else{
  Y_eigengenes <- Y$modules$MEs
}
```


Samples as rows, modules as columns

```{r}
dim(Y_eigengenes); dim(X_eigengenes)
```

```{r}
# Check that the samples are in the same order. 
# If they are not in order, change their order to match; If they do not match one-to-one, call an error.
same_order <- all(rownames(Y_eigengenes) == rownames(X_eigengenes))
if(!same_order){
  Y_eigengenes <- Y_eigengenes[order(rownames(Y_eigengenes)),]
  X_eigengenes <- X_eigengenes[order(rownames(X_eigengenes)),]
  same_order <- all(rownames(Y_eigengenes) == rownames(X_eigengenes))
  if(!same_order){
    stop("Sample names do not match. Samples should be identical.", call. = F)
  }
} else{cat("Samples match")}
```



```{r}
p.value_matr <- corr.value_matr <- matrix(ncol = ncol(Y_eigengenes), 
                                          nrow = ncol(X_eigengenes), 
                                          dimnames = list(colnames(X_eigengenes), 
                                                          colnames(Y_eigengenes)))


for(i in 1:ncol(X_eigengenes)){
  for(j in 1:ncol(Y_eigengenes)){
    cor.res <- cor.test(X_eigengenes[,i], Y_eigengenes[,j])
    p.value_matr[i, j] <- cor.res$p.value
    corr.value_matr[i, j] <- cor.res$estimate
  }
}

# Correct for number of tests
p.value_matr.adjust <- p.adjust(p.value_matr, method = "fdr")
dim(p.value_matr.adjust) <- dim(p.value_matr)
dimnames(p.value_matr.adjust) <- list(colnames(X_eigengenes), colnames(Y_eigengenes))


# Add significance level.  
# One star means a p-value of less than 0.05; Two stars is less than 0.01, and three, is less than 0.001.

signif_matrix <- rep("", length(p.value_matr))
three_star <- which( p.value_matr <= 0.001)
signif_matrix[three_star] <- "***"
two_star <- which((p.value_matr <= 0.01) & (p.value_matr > 0.001))
signif_matrix[two_star] <- "**"
one_star <- which((p.value_matr <= 0.05) & (p.value_matr > 0.01))
signif_matrix[one_star] <- "*"
dim(signif_matrix) = dim(p.value_matr) # Give textMatrix the correct dimensions 


# Collect all results into a list.
Y_corr_X <- list(p_value = p.value_matr, 
                 p_value_adj = p.value_matr.adjust,
                 signif_matrix = signif_matrix,
                 correlation = corr.value_matr)
rm(p.value_matr, p.value_matr.adjust, signif_matrix, corr.value_matr)
```


```{r fig.height=7, fig.width=3}
heatmap_colors <- colorRampPalette(rev(RColorBrewer::brewer.pal(n = 6, name ="RdBu")))(51)

pheatmap::pheatmap(Y_corr_X$correlation, 
                   color = heatmap_colors, 
                   treeheight_col = 0, 
                   treeheight_row = 0,  # will be shown on the transcriptomics ME heatmap
                   cluster_rows = X_ME_dendro,
                   cutree_rows = row_cut,
                   display_numbers = Y_corr_X$signif_matrix, 
                   fontsize_number = 8, #10
                   breaks = seq(from = -1, to = 1, length.out = 51), 
                   silent = F,
                   show_rownames = F,
                   labels_row = paste0(prefix_OTUs, rownames(Y_corr_X$correlation)),
                   labels_col = paste0(prefix_OTUs, colnames(Y_corr_X$correlation)),
                   main = "EigenOTUs") -> Y_corr_X_plot
```

# Correlate transcriptomics with traits

Load in traits and external variables.

```{r}
(load(file = "../data/RData/sampleMeta.RData"))
```

We simplify by only chosing the variables we need and samples that are available in every omics type.
```{r}
(load(file = "../data/RData/samples_to_keep.RData")) # All samples have been intersected. Gut and Liver are the same. Only diff is the letter G or L.
```

```{r}
# simplify by chosing only the columns we need.
sample_info.meta %>% dplyr::select(sampleName, Water, Day, Feed, Host_sex, Condition_factor) %>% 
  filter(sampleName %in% gut_samples_to_keep) -> Z_traits
Z_traits
```
The variable `Condition factor` was calculated as $ CF = \frac{W \cdot 10^N}{L^3}  $, where W si weight in grams, L is length in mm and N is a scaling factor. An N of 4 was used in this case.

To be able to correlate some of these variables they need to be converted to factors.
Because day 9 in freshwater is not the same day as day 9 in saltwater we need to recode Day appropriately by first appending FW or SW to the days number then converting to a factor. 

```{r}
Z_traits %>% 
  mutate(Host_sex = factor(Host_sex, levels = c("male", "female"))) %>% 
  mutate(Water = factor(Water)) %>%
  mutate(Day = ifelse(Water == "FW", paste0("FW", Day), paste0("SW", Day))) %>%
  mutate(Day = factor(Day, levels= c(paste0("FW", c(0,6,9,16,20)), paste0("SW", c(0,1,2,6,9,16,20))))) %>%
  mutate(Feed = factor(Feed, levels = c("FO", "VOFO", "FOVO", "VO"))) -> Z_traits
Z_traits
```

One hot encoding the nominal variables.
```{r}
Z_traits %>%
  dplyr::select(one_of("sampleName","Feed", "Water", "Host_sex")) %>% 
  column_to_rownames(var = "sampleName") %>% 
  model.matrix(~.-1, data = .) %>% 
  as.data.frame() %>% 
  rownames_to_column(var = "sampleName") %>% 
  as_tibble() -> nominal_variables
nominal_variables
```

```{r}
Z_traits %>% 
  dplyr::select(one_of("sampleName", "Day", "Condition_factor")) %>% 
  mutate(Day = as.integer(Day)) -> ordinal_and_other_variables
ordinal_and_other_variables
```

Write over the unconverted table with the numerically converted variables.
```{r}
Z_traits <- full_join(nominal_variables, ordinal_and_other_variables, by = "sampleName")

if(take_average){
  Z_traits %>% 
  mutate(sampleName = sub("_[0-9]{1,2}$", "", sampleName)) %>% 
  group_by(sampleName) %>% 
  summarise_all(list(mean), na.rm = TRUE) %>%  # Remove the NA values 
  ungroup() %>% 
  dplyr::rename(Female_ratio = Host_sexfemale) %>% 
  dplyr::select(sampleName, c(starts_with("Feed"), Day, WaterSW, Female_ratio, Condition_factor)) %>% 
  tibble::column_to_rownames(var = "sampleName") -> Z_traits
  
  Z_traits <- as.data.frame(Z_traits)
  
} else{
  Z_traits <- Z_traits %>% 
    dplyr::select(sampleName, c(starts_with("Feed"), Day, WaterSW, Host_sexfemale, Condition_factor)) %>%
    column_to_rownames(var = "sampleName")
  
  Z_traits <- as.data.frame(Z_traits)
}

Z_traits %>% rmarkdown::paged_table()
```


```{r}
# Check that the samples are in the same order. 
# If they are not in order, change their order to match; If they do not match one-to-one, call an error.
same_order <- all(rownames(Z_traits) == rownames(X_eigengenes))
if(!same_order){
  Y_eigengenes <- Y_eigengenes[order(rownames(Y_eigengenes)),]
  X_eigengenes <- X_eigengenes[order(rownames(X_eigengenes)),]
  same_order <- all(rownames(Y_eigengenes) == rownames(X_eigengenes))
  if(!same_order){
    stop("Sample names do not match. Samples should be identical.", call. = F)
  }
} else{cat("Samples match")}
```

## Correlation

Four variables are categorical:  

- `Day` is ordinal, i.e. it has a natural order, day one comes before day two etc.  

Three are nominal:  

- Some are naturally dichotmous, like the sex of fish, `Host_sex`, and the water environment, `Water`. These can be encoded 0 and 1.  

- `Feed` has more than two levels but can be dichotomised using a one hot encoding.  

One is continuous:  

- `Condition factor`  


All are going to be correlated to module eigengenes which are continuous.  

Ordinal x continuous = Spearman correlation  

Nominal x continuous = (one hot encoding, then) point biserial  
The point biserial correlation is a special case of pearson correlation where one variable is dichotmous and the other is continuous.  

continuous x continuous = pearson correlation  


Averaging changes the host_sex variable into a proportion of females, or `Female_ratio` variable.  
The correlation then no longer becomes a point biserial, but since both uses pearson correlation there is no need to change the correlation measure.  


<!-- There seems to be no extreme bias in female ratio from FW to SW -->
<!-- There are more females in SW than in FW, but this might be as well be random. Still, if there is a general trend from FW to FW for the non averaged data, then you should expect that female ratio also correlates to the saltwater transition. -->
<!-- ```{r} -->
<!-- Z_traits[which(grepl("_FW_",rownames(Z_traits))), ]$Host_sexfemale %>% hist() -->
<!-- Z_traits[which(grepl("_SW_",rownames(Z_traits))), ]$Host_sexfemale %>% hist() -->
<!-- ``` -->


```{r}
p.value_matr <- corr.value_matr <- matrix(ncol = ncol(Z_traits), 
                                          nrow = ncol(X_eigengenes), 
                                          dimnames = list(colnames(X_eigengenes), 
                                                          colnames(Z_traits)))
for(i in 1:ncol(X_eigengenes)){
  for(j in 1:ncol(Z_traits)){
    
    if(colnames(Z_traits)[j] == "Day"){
      cor.res <- cor.test(X_eigengenes[,i], Z_traits[,j], method = "spearman", exact = FALSE)
      p.value_matr[i, j] <- cor.res$p.value
      corr.value_matr[i, j] <- cor.res$estimate
    } else{
      cor.res <- cor.test(X_eigengenes[,i], Z_traits[,j])
      p.value_matr[i, j] <- cor.res$p.value
      corr.value_matr[i, j] <- cor.res$estimate
    }
  }
}


# Correct for number of tests
p.value_matr.adjust <- p.adjust(p.value_matr, method = "fdr")
dim(p.value_matr.adjust) <- dim(p.value_matr)
dimnames(p.value_matr.adjust) <- list(colnames(X_eigengenes), colnames(Z_traits))


# Add significance level.  
# One star means a p-value of less than 0.05; Two stars is less than 0.01, and three, is less than 0.001.

signif_matrix <- rep("", length(p.value_matr))
three_star <- which( p.value_matr <= 0.001)
signif_matrix[three_star] <- "***"
two_star <- which((p.value_matr <= 0.01) & (p.value_matr > 0.001))
signif_matrix[two_star] <- "**"
one_star <- which((p.value_matr <= 0.05) & (p.value_matr > 0.01))
signif_matrix[one_star] <- "*"
dim(signif_matrix) = dim(p.value_matr) # Give textMatrix the correct dimensions 


# Collect all results into a list.
Z_corr_X <- list(p_value = p.value_matr, 
                 p_value_adj = p.value_matr.adjust,
                 signif_matrix = signif_matrix,
                 correlation = corr.value_matr)
rm(p.value_matr, p.value_matr.adjust, signif_matrix, corr.value_matr)
```


```{r fig.height=7, fig.width=3}
heatmap_colors <- colorRampPalette(rev(RColorBrewer::brewer.pal(n = 6, name ="RdBu")))(51)

pheatmap::pheatmap(Z_corr_X$correlation, 
                   color = heatmap_colors, 
                   treeheight_col = 0, 
                   treeheight_row = 0, # will be shown on the transcriptomics ME heatmap
                   cluster_rows = X_ME_dendro,
                   cutree_rows = row_cut,
                   display_numbers = Z_corr_X$signif_matrix, 
                   fontsize_number = 8, # 10
                   breaks = seq(from = -1, to = 1, length.out = 51), 
                   silent = F, 
                   legend = F,
                   show_rownames = F,
                   main = "Traits and external variables") -> Z_corr_X_plot

# Modules with no significant associations
if (filter.sig.mod == 1) {
  idx1 <- rowSums(#Z_corr_X$signif_matrix == "*" |
                 Z_corr_X$signif_matrix == "**" |
                 Z_corr_X$signif_matrix == "***") > 0
  idx2 <- rowSums(#Y_corr_X$signif_matrix == "*" |
                  Y_corr_X$signif_matrix == "**" | 
                  Y_corr_X$signif_matrix == "***") > 0
  sig.modules.h <- rownames(Y_corr_X$p_value)[idx1 & idx2]
  
  sig.modules.m <- colnames(Y_corr_X$p_value)[colSums(#Y_corr_X$signif_matrix[idx1 & idx2,] == "*" |
                                                     Y_corr_X$signif_matrix[idx1 & idx2,] == "**" | 
                                                     Y_corr_X$signif_matrix[idx1 & idx2,] == "***") > 0]
  
  
  save(sig.modules.h, sig.modules.m, file = "../data/module_data/sig_modules.Rdata")
}
```



# Combine heatmaps


```{r fig.height=10, fig.width=15}
cowplot::plot_grid(Z_corr_X_plot$gtable,
                   X_plot$gtable,
                   Y_corr_X_plot$gtable, 
                   ncol = 3, 
                   rel_widths = c(dim(Z_traits)[2]/3, 
                   ifelse(take_average,dim(X_eigengenes)[1]/3,dim(X_eigengenes)[1]/9),
                   dim(Y_eigengenes)[2]/2.2),
                   align = "h") + ggplot2::theme(plot.margin = ggplot2::unit(c(3,0,2.5,1), "cm")) # Top, Right, Bottom, Left
```



```{r message=FALSE, warning=FALSE, include=FALSE}
save_path_plot <- paste("../images/split_heatmaps/splitHeatmap", 
                        "Modules",
                        ifelse(take_average, "avg","rep"), 
                        sub("\\.", "_", X$identity), 
                        sub("\\.", "_", Y$identity),
                        sep = "_-_")

ggplot2::ggsave(filename = paste0(save_path_plot, ".pdf"), width = 15, height = 4.5, device = "pdf")
```


# Stripcharts and scatterplots

```{r}
library(ggplot2)
library(cowplot)
```


```{r}

if(take_average){
  Z_traits %>% 
  rownames_to_column(var = "sampleName") %>% 
  dplyr::select(sampleName, Female_ratio, Condition_factor)-> Z_t
} else{
  Z_traits %>% 
  rownames_to_column(var = "sampleName") %>% 
  dplyr::select(sampleName, Host_sexfemale, Condition_factor)-> Z_t
}


X_e <- X_eigengenes
colnames(X_e) <- paste0("h",colnames(X_e))
X_e %>% rownames_to_column(var = "sampleName") -> X_e

Y_e <- Y_eigengenes
colnames(Y_e) <- paste0("m",colnames(Y_e))
Y_e %>% rownames_to_column(var = "sampleName") -> Y_e


A <- inner_join(Z_t, X_e, by = "sampleName") %>% 
  inner_join(Y_e, by = "sampleName") %>% 
  tidyr::separate(col = "sampleName", 
                  into = c("Tissue", "Water", "Day", "Feed"), remove = F) %>% 
  dplyr::select(-Tissue) %>% 
  mutate(Feed = factor(Feed, levels = c("FO", "VOFO", "FOVO", "VO"))) %>% 
  mutate(Day = as.numeric(sub("D", "", Day)))

A %>% rmarkdown::paged_table()
```


